//
//  FeedTableViewCell.swift
//  iVideos
//
//  Created by Devi on 03/04/18.
//  Copyright © 2018 Test. All rights reserved.
//

//MARK: Header Files

import UIKit

class FeedTableViewCell: UITableViewCell {
    
    //MARK: Outlets
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
