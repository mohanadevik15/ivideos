//
//  DisplayViewController.swift
//  iVideos
//
//  Created by Devi on 03/04/18.
//  Copyright © 2018 Test. All rights reserved.
//

//MARK: Header Files

import UIKit
import AVKit
import AVFoundation

class DisplayViewController: UIViewController {
    
    //MARK: Outlets
    
    @IBOutlet weak var playerView: UIView!
    
    //MARK: Properties
    
    var video : Video?
    
    //MARK: View LifeCyle
    
    //DidLoad()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.video?.name ?? ""
    }
    
    //DidAppear()
    
    override func viewDidAppear(_ animated: Bool) {
        self.loadVideo()
    }
    
    //MARK: Helper Methods
    
    func loadVideo() {
        if let urlStr = video?.url {
            let player = AVPlayer(url: URL(string:urlStr)!)
            let playerController = AVPlayerViewController()
            playerController.showsPlaybackControls = true
            playerController.player = player
            
            // add as child viewcontroller
            self.addChildViewController(playerController)
            self.playerView.addSubview(playerController.view)
            playerController.view.frame = self.view.bounds
            playerController.didMove(toParentViewController: self)
            player.play()
        }
    }
}
