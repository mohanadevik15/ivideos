//
//  ViewController.swift
//  iVideos
//
//  Created by Devi on 03/04/18.
//  Copyright © 2018 Test. All rights reserved.
//

//MARK: Header Files

import UIKit
import ObjectMapper
import SDWebImage

class HomeViewController: UIViewController {
    
    //MARK: Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    //MARK: Properties
    
    var videoList = [Video]()
    
    //MARK: View LifeCycle
    
    //DidLoad()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.loadData()
    }
    
    //MARK: Helper Methods
    
    func loadData() {
        self.activityIndicatorView.startAnimating()
        Services.getFeeds { (response) in
            self.activityIndicatorView.stopAnimating()  
            if let videoList = Mapper<VideoList>().map(JSONObject: response.result.value), let videos = videoList.videos {
                self.videoList = videos.sorted(by: { (video1, video2) -> Bool in
                    if let indexStr1 = video1.index, let indexStr2 = video2.index, let index1 = Int(indexStr1), let index2 = Int(indexStr2)  {
                        return (index1 < index2)
                    }
                    else {
                        return false
                    }
                })
                self.tableView.reloadData()
            }
        }
    }
}

//MARK: Extension Class

//TableViewDatasource and Delegate Methods

extension HomeViewController : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.videoList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 155.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedTableViewCell", for: indexPath) as! FeedTableViewCell
        let video = self.videoList[indexPath.row]
        if let image = video.image, let title = video.name {
            cell.imgView.sd_setImage(with: URL(string: image), placeholderImage: nil, options: .highPriority, progress: nil, completed: nil)
            cell.lblTitle.text = title
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let video = self.videoList[indexPath.row]
        if let DVc = self.storyboard?.instantiateViewController(withIdentifier: "DisplayViewController") as? DisplayViewController {
            DVc.video = video
            self.navigationController?.pushViewController(DVc, animated: true)
        }
    }
}


