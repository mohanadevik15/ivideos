//
//  Services.swift
// iVideos
//
//  Created by Devi on 03/04/18.
//  Copyright © 2018 Test. All rights reserved.
//

//MARK: Header Files

import UIKit
import Alamofire
import ObjectMapper

typealias responseHandler = (DataResponse<Any>) -> Void


class Services {
    static func makeGETCall(urlString : String, parameters: [String : AnyObject]? = nil,
                            completionHandler : @escaping responseHandler, errorHandler: responseHandler? = nil) {
       
        let percentageURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        print("urlString", urlString)
        
        Alamofire.request(percentageURL, parameters: parameters)
            .responseJSON(completionHandler: { response in
                print("Response" ,response.result.value ?? "", response.response?.statusCode ?? 0)
                switch response.result {
                case .success( _):
                        completionHandler(response)
                case .failure( let error):
                    print("SERVICE Error Message : " + error.localizedDescription)
                }
            })
    }
}
