//
//  WebServices.swift
//  iVideos

//  Created by Devi on 03/04/18.
//  Copyright © 2018 Test. All rights reserved.

//MARK: Header Files

import UIKit

let GET_VIDEO_FEEDS = "http://ventunotech.com/tv/tasks/test1.php"

extension Services {
    
    static func getFeeds(completionHandler : @escaping responseHandler) {
       makeGETCall(urlString: GET_VIDEO_FEEDS, completionHandler: completionHandler)
    }
}
