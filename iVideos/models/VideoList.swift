//
//  VideoList.swift
//  iVideos
//
//  Created by Devi on 03/04/18.
//  Copyright © 2018 Test. All rights reserved.
//

//MARK: Header Files

import UIKit
import ObjectMapper

class VideoList: Mappable {
    
    var videos: Array<Video>?
    
    required init(map : Map) {
    }
    
    func mapping(map: Map) {
        videos <- map["videos"]
    }

}
