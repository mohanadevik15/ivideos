//
//  Video.swift
//  iVideos
//
//  Created by Devi on 03/04/18.
//  Copyright © 2018 Test. All rights reserved.
//

//MARK: Header Files

import UIKit
import ObjectMapper

class Video: Mappable {
    
    var index : String?
    var name : String?
    var image: String?
    var url : String?
    
    required init(map : Map) {
    }
    
    func mapping(map: Map) {
        index <- map["index"]
        name <- map["name"]
        image <- map["image"]
        url <- map["url"]
    }
    

}
